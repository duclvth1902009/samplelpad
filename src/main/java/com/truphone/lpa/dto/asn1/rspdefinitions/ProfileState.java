/**
 * This class file was automatically generated by jASN1 v1.11.3 (http://www.beanit.com)
 */

package com.truphone.lpa.dto.asn1.rspdefinitions;

import com.beanit.jasn1.ber.*;
import com.beanit.jasn1.ber.types.*;
import com.beanit.jasn1.ber.types.string.*;

import java.math.BigInteger;

public class ProfileState extends BerInteger {

	private static final long serialVersionUID = 1L;

	public ProfileState() {
	}

	public ProfileState(byte[] code) {
		super(code);
	}

	public ProfileState(BigInteger value) {
		super(value);
	}

	public ProfileState(long value) {
		super(value);
	}

}
