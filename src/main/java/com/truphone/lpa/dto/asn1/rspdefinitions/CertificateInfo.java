/**
 * This class file was automatically generated by jASN1 v1.11.3 (http://www.beanit.com)
 */

package com.truphone.lpa.dto.asn1.rspdefinitions;

import com.beanit.jasn1.ber.*;
import com.beanit.jasn1.ber.types.*;
import com.beanit.jasn1.ber.types.string.*;

public class CertificateInfo extends BerBitString {

	private static final long serialVersionUID = 1L;

	public CertificateInfo() {
	}

	public CertificateInfo(byte[] code) {
		super(code);
	}

	public CertificateInfo(byte[] value, int numBits) {
		super(value, numBits);
	}

	public CertificateInfo(boolean[] value) {
		super(value);
	}

}
