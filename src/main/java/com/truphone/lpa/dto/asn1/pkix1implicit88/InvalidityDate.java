/**
 * This class file was automatically generated by jASN1 v1.11.3 (http://www.beanit.com)
 */

package com.truphone.lpa.dto.asn1.pkix1implicit88;

import com.beanit.jasn1.ber.types.BerGeneralizedTime;

public class InvalidityDate extends BerGeneralizedTime {

	private static final long serialVersionUID = 1L;

	public InvalidityDate() {
	}

	public InvalidityDate(byte[] value) {
		super(value);
	}

}
