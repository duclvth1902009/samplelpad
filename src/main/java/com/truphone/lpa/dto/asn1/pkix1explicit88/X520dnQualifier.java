/**
 * This class file was automatically generated by jASN1 v1.11.3 (http://www.beanit.com)
 */

package com.truphone.lpa.dto.asn1.pkix1explicit88;

import com.beanit.jasn1.ber.types.string.BerPrintableString;


public class X520dnQualifier extends BerPrintableString {

	private static final long serialVersionUID = 1L;

	public X520dnQualifier() {
	}

	public X520dnQualifier(byte[] value) {
		super(value);
	}

}
